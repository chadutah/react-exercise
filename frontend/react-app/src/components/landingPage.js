import React, { useState, useEffect } from 'react';
import Data from '../utilities/dataApi';
import { setInterval } from 'timers';
import Modal from 'react-awesome-modal';

export default function LandingPage() {

    const [value, setValue] = useState(null)
    const [location, setLocation] = useState(null)
    const [list, setList] = useState(null)
    const [info, setInfo] = useState('')
    const [visible, setVisible] = useState(false)

    const handleChange = (event) => {
        setValue(event.target.value);
    }
    const handleChangeState = (event) => {
        setLocation(event.target.value);
    }
    const search = (event) => {
        if (value !== null && location !== null) {
            if (value === "representative") {
                fetch(`http://localhost:3000/representatives/${location}`)
                    .then(response => response.json())
                    .then(data => setList(data));
            } else {
                fetch(`http://localhost:3000/senators/${location}`)
                    .then(response => response.json())
                    .then(data => setList(data));
            }
        } else {
            alert("must select who and where")
        }
    }
    const moreInfo = (index) => {
        console.log(index)
        setInfo(list.results[index])
        openModal()
        console.log(list)

    }
    const openModal = () => {
        setVisible(true)
    }

    const closeModal = () => {
        setVisible(false)
    }
    return (
        <div>
            <br />
            <div style={{
                width: "50vw", height: "300px", margin: "0 auto", background: "white", borderRadius: "25px",
                boxShadow: "0 0 2.8px 2.2px rgba(0, 0, 0, 0.034),0 0 6px 3px rgba(0, 0, 0, 0.048),0 0 2px 1px rgba(0, 0, 0, 0.06),0 0 2.3px 1.9px rgba(0, 0, 0, 0.072),0 0 4.8px 3.4px rgba(0, 0, 0, 0.086),0 0 10px 8px rgba(0, 0, 0, 0.12)"
            }}>
            <br/>
                <h1>Find your Representatives</h1>
                <br />
                <br />
                <form>
                    <label>
                        Search by:
          <select style={{ marginLeft: '5px' }} onChange={handleChange} defaultValue="DEFAULT">
                            <option value="DEFAULT" disabled hidden> Select one</option>
                            <option value="representative">Representative</option>
                            <option value="senator">Senator</option>
                        </select>
                    </label>
                    <br />
                    <br />
                    <label>
                        Select State:
          <select style={{ marginLeft: '5px' }} defaultValue="DEFAULT" onChange={handleChangeState}>
                            <option value="DEFAULT" disabled hidden> Select one</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AR">Arizona</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="NY">New York</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                        </select>
                    </label>
                </form>
                <br />
                <br />
                <button onClick={() => search()}>SEARCH</button>
                <br />
            </div>
            <br />
            {list !== null ? list.results.map(function (person, index) {
                return (<div key={index} onClick={() => moreInfo(index)}>
                    <h2 style={{ textShadow: "1px 1px 0px rgba(0,0,0,0.15)" }}>{person.name}</h2>
                    <p style={{ marginTop: "-20px" }}>{person.party}</p>
                </div>
                )
            }) : null}
            <section>
                <Modal visible={visible} width="600" height="300" effect="fadeInUp" onClickAway={() => closeModal()}>
                    <div>
                        <h1>{info.name}</h1>
                        <p>{info.party}</p>
                        <p>District: {info.district}</p>
                        <a href={info.link}>{info.link}</a>
                        <p>{info.office}</p>
                        <p>{info.phone}</p>
                        <p style={{ color: "blue" }} onClick={() => closeModal()}>Close</p>
                    </div>
                </Modal>
            </section>
        </div>
    )
}